#!/usr/bin/env bash

rm -f -r ~/.local/share/nvim
rm -f -r ~/.local/state/nvim
rm -f -r ~/.cache/nvim
rm -f -r ~/.cache/ci_lint_cache

