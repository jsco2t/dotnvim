return {
	"nvimtools/none-ls.nvim",
	config = function()
		local null_ls = require("null-ls")
		null_ls.setup({
			sources = {
				null_ls.builtins.formatting.stylua,
				null_ls.builtins.formatting.mdformat,
				null_ls.builtins.completion.spell,
				null_ls.builtins.diagnostics.codespell,
				null_ls.builtins.formatting.prettier.with({
					extra_filetypes = { "toml", "yaml", "json", "js", "markdown", "ts" },
				}),
				null_ls.builtins.code_actions.gitsigns,
				null_ls.builtins.diagnostics.pylint,
				null_ls.builtins.diagnostics.buf,
				null_ls.builtins.diagnostics.editorconfig_checker,
				--null_ls.builtins.diagnostics.golangci_lint, -- handled by LSP currently
				--null_ls.builtins.diagnostics.staticcheck, -- advanced go linter -- handled by LSP currently
				null_ls.builtins.diagnostics.markdownlint,
				--null_ls.builtins.diagnostics.shellcheck, -- managed by LSP currently
				null_ls.builtins.formatting.black,
				null_ls.builtins.formatting.isort,
			},
			on_attach = function(client, bufnr)
				if client.supports_method("textDocument/formatting") then
					vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
					vim.api.nvim_create_autocmd("BufWritePre", {
						group = augroup,
						buffer = bufnr,
						callback = function()
							-- on 0.8, you should use vim.lsp.buf.format({ bufnr = bufnr }) instead
							-- on later neovim version, you should use vim.lsp.buf.format({ async = false }) instead
							vim.lsp.buf.format({ async = false })
							--vim.lsp.buf.formatting_sync()
						end,
					})
				end
			end,
		})

		vim.keymap.set("n", "<leader>gf", vim.lsp.buf.format, {})
	end,
}
