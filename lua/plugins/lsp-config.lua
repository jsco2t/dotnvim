return {
	-- installer for lsp's
	{
		"williamboman/mason.nvim",
		config = function()
			require("mason").setup({
				registries = {
					"github:mason-org/mason-registry",
				},
			})
		end,
	},
	-- allows for default config including ensuring a baseline set of lsp's exist
	{
		"williamboman/mason-lspconfig.nvim",
		config = function()
			require("mason-lspconfig").setup({
				ensure_installed = { -- see `lspconfig.*.setup({})` calls in `nvim-lspconfig`
					"ansiblels",
					"bashls",
					"bufls",
					"dockerls",
					"eslint",
					"golangci_lint_ls",
					"gopls",
					--"helm_ls", -- not supported on linux/arm (error on rpi)
					"jsonls",
					"lua_ls",
					"marksman",
					"pyright",
					"rust_analyzer",
					"terraformls",
					"tsserver",
					"yamlls",
				},
			})
		end,
	},
	-- alternate mason tool installer (formatters, linters...etc)
	{
		"whoissethdaniel/mason-tool-installer.nvim",
		config = function()
			require("mason-tool-installer").setup({
				ensure_installed = {
					"black",
					"isort",
					"cspell",
					"codespell",
					"stylua",
					"shellcheck",
					"staticcheck",
					"editorconfig-checker",
					"gofumpt",
					"goimports",
					"prettier",
					"pylint",
					"protolint",
					"jsonlint",
					"markdownlint",
				},
			})
		end,
	},
	-- bridges gap between lsp and nvim (wires the two up)
	{
		"neovim/nvim-lspconfig",
		config = function()
			local capabilities = require("cmp_nvim_lsp").default_capabilities()
			capabilities.textDocument.completion.completionItem.snippetSupport = true
			local lspconfig = require("lspconfig")

			-- these language setup calls need to be paired with `ensure_installed`, more info: `:h lspconfig-setup`
			lspconfig.ansiblels.setup({ capabilities = capabilities })
			lspconfig.bashls.setup({ capabilities = capabilities })
			lspconfig.bufls.setup({ capabilities = capabilities })
			lspconfig.dockerls.setup({ capabilities = capabilities })
			lspconfig.eslint.setup({ capabilities = capabilities })
			lspconfig.golangci_lint_ls.setup({ capabilities = capabilities })
			--lspconfig.gopls.setup({ capabilities = capabilities })
			lspconfig.helm_ls.setup({ capabilities = capabilities })
			lspconfig.jsonls.setup({ capabilities = capabilities })
			lspconfig.lua_ls.setup({ capabilities = capabilities })
			lspconfig.marksman.setup({ capabilities = capabilities })
			lspconfig.pyright.setup({ capabilities = capabilities })
			lspconfig.rust_analyzer.setup({ capabilities = capabilities })
			lspconfig.terraformls.setup({ capabilities = capabilities })
			lspconfig.tsserver.setup({ capabilities = capabilities })
			lspconfig.yamlls.setup({ capabilities = capabilities })

			lspconfig.gopls.setup({
				capabilities = capabilities,
				flags = { debounce_text_changes = 200 },
				settings = {
					gopls = {
						usePlaceholders = true,
						gofumpt = true,
						analyses = {
							nilness = true,
							unusedparams = true,
							unusedwrite = true,
							useany = true,
						},
						codelenses = {
							gc_details = false,
							generate = true,
							regenerate_cgo = true,
							run_govulncheck = true,
							test = true,
							tidy = true,
							upgrade_dependency = true,
							vendor = true,
						},
						experimentalPostfixCompletions = true,
						completeUnimported = true,
						staticcheck = true,
						directoryFilters = { "-.git", "-node_modules" },
						semanticTokens = true,
						hints = {
							assignVariableTypes = true,
							compositeLiteralFields = true,
							compositeLiteralTypes = true,
							constantValues = true,
							functionTypeParameters = true,
							parameterNames = true,
							rangeVariableTypes = true,
						},
					},
				},
			})

			vim.keymap.set("n", "K", vim.lsp.buf.hover, {})
			vim.keymap.set("n", "<leader>gd", vim.lsp.buf.definition, {})
			vim.keymap.set("n", "<leader>gr", vim.lsp.buf.references, {})
			vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, {})
		end,
	},
}
