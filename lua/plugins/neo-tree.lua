return {
	{
		"nvim-neo-tree/neo-tree.nvim",
		branch = "v3.x",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
			"MunifTanjim/nui.nvim",
		},
		keys = {
			{ "<leader>e", "<cmd>Neotree focus<cr>", desc = "Neotree focus" },
			{ "<leader>he", "<cmd>Neotree close<cr>", desc = "Neotree close" },
		},
		config = function()
			require("neo-tree").setup({
				window = {
					width = 30,
				},
				filesystem = {
					filtered_items = {
						hide_hidden = false,
						hide_dotfiles = false,
						hide_gitignored = false,
						never_show = {
							".DS_Store",
							"thumbs.db",
						},
					},
					follow_current_file = {
						enabled = true,
					},
				},
			})
		end,
	},
}
