return {
	{
		-- "fatih/vim-go",
		-- --priority = 1000,
		-- dependencies = {
		--   "charlespascoe/vim-go-syntax",
		-- },
		-- config = function()
		--   require("vim-go").setup()
		-- end,
	},
	{
		"ray-x/go.nvim",
		dependencies = { -- optional packages
			"ray-x/guihua.lua",
			"neovim/nvim-lspconfig",
			"nvim-treesitter/nvim-treesitter",
		},
		config = function()
			require("go").setup()
		end,
		event = { "CmdlineEnter" },
		ft = { "go", "gomod" },
		build = ':lua require("go.install").update_all_sync()', -- if you need to install/update all binaries
	},
}
