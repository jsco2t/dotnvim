return {
  'nvim-treesitter/nvim-treesitter',
  build = ":TSUpdate",
  config = function()
    local config = require('nvim-treesitter.configs')
    config.setup({
      auto_install = true,
      ensure_installed = {
        "bash",
        "diff",
        "dockerfile",
        "git_config",
        "git_rebase",
        "gitattributes",
        "gitcommit",
        "gitignore",
        "go",
        "gomod",
        "gosum",
        "gowork",
        "hcl",
        "javascript",
        "json",
        "lua",
        "markdown",
        "markdown_inline",
        "proto",
        "python",
        "rust",
        "sql",
        "terraform",
        "toml",
        "typescript",
        "yaml"
      },
      sync_install = false,    -- forces synchronous install
      highlight = { enabled = true },
      indent = { enable = true },
    })
  end
}
