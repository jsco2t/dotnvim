vim.g.mapleader = " "
vim.g.autoformat = true

local opt = vim.opt

opt.mouse = "a" -- Enable mouse mode
opt.number = true -- Print line number
opt.signcolumn = "yes" -- Always show the signcolumn, otherwise it would shift the text each time
opt.smartcase = true -- Don't ignore case with capitals
opt.smartindent = true -- Insert indents automatically
opt.spelllang = { "en" }
opt.splitbelow = true -- Put new windows below current
opt.splitkeep = "screen"
opt.splitright = true -- Put new windows right of current
opt.tabstop = 2 -- Number of spaces tabs count for
opt.termguicolors = true -- True color support
opt.expandtab = true -- Use spaces instead of tabs
opt.clipboard = "unnamedplus" -- Sync with system clipboard
opt.cursorline = true -- Enable highlighting of the current line
opt.shiftwidth = 2 -- Size of an indent
opt.virtualedit = "block" -- Allow cursor to move where there is no text in visual block mode
opt.linebreak = true -- tells nvim to break at word boundaries
opt.wrap = true -- turns on soft wrapping
opt.showbreak = "➩"
opt.textwidth = 0 -- disables hard text wrapping
opt.wrapmargin = 0 -- disables hard text wrapping (need `textwidth=0` as well - see above)
opt.breakindent = true -- maintain indent when wrapping indented lines
opt.listchars = { tab = " ˗", trail = "·", space = "◦" } -- whitespace replacement characters
opt.list = true -- show whitespace characters
vim.o.sessionoptions = "curdir,folds,terminal"

vim.notify = function(msg, log_level, _opts)
	if msg:match("exit code") then
		return
	end
	if log_level == vim.log.levels.ERROR then
		vim.api.nvim_err_writeln(msg)
	else
		vim.api.nvim_echo({ { msg } }, true, {})
	end
end
