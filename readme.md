# nvim config

## background

My personal `dotfiles` pattern is based on using `git` as the storage mechanism for the dotfiles.
The pattern is similar to what's described on the following sites:

https://wiki.archlinux.org/title/Dotfiles

https://www.atlassian.com/git/tutorials/dotfiles

## nvim config

My `nvim config` is stored as a public repo and I deploy it as a `git submodule` in my `dotfiles`.

The setup for this is pretty simple. First - make sure any pre-existing `nvim config` has been removed:

```bash
rm -f -r ~/.cache/nvim
rm -f -r ~/.local/**/nvim
rm -f -r ~/.config/nvim
```
Then add the submodule to pull down the `nvim config`

```bash
git submodule add https://gitlab.com/jsco2t/dotnvim.git .config/nvim

#  --or--

git submodule add git@gitlab.com:jsco2t/dotnvim.git .config/nvim

#  -- then --

cd .config/nvim
git pull
```

